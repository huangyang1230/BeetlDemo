import com.hy.demo.CommodityEntity;
import com.hy.demo.ThreadBeetl;
import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.resource.ClasspathResourceLoader;
import org.junit.Test;

import java.io.IOException;

/**
 * @author ocean
 * @create 2018-06-28-下午5:18
 * @description TODO
 */
public class BeetlTest {
    /**
     * 测试beetl的多线程操作是否出现安全性
     * @throws IOException
     */
    @Test
    public void test1() throws Exception {
        ClasspathResourceLoader classpathResourceLoader = new ClasspathResourceLoader("/rule");
        Configuration cfg = Configuration.defaultConfiguration();
        GroupTemplate gt = new GroupTemplate(classpathResourceLoader, cfg);
        //多执行几次会出现传入的值和传出的值不匹配
        for (int i = 0; i < 100; i++) {
            CommodityEntity commodityEntity = new CommodityEntity("书名_" + i, (double) i, "ISBN_" + i);
            ThreadBeetl threadBeetl = new ThreadBeetl(commodityEntity, gt);
            new Thread(threadBeetl).start();
        }
//        Thread.currentThread().sleep(1000*5);
    }

    public static void main(String[] args) {
        ClasspathResourceLoader classpathResourceLoader = new ClasspathResourceLoader("rule");
        Configuration cfg = null;
        try {
            cfg = Configuration.defaultConfiguration();
        } catch (IOException e) {
            e.printStackTrace();
        }
        GroupTemplate gt = new GroupTemplate(classpathResourceLoader, cfg);
        //多执行几次会出现传入的值和传出的值不匹配
        for (int i = 0; i < 100; i++) {
            CommodityEntity commodityEntity = new CommodityEntity("书名_" + i, (double) i, "ISBN_" + i);
            ThreadBeetl threadBeetl = new ThreadBeetl(commodityEntity, gt);
            new Thread(threadBeetl).start();
        }

    }

}
