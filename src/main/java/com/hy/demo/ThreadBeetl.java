package com.hy.demo;

import java.util.HashMap;
import java.util.Map;

import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.exception.ScriptEvalError;

/**
 * @author ocean
 * @create 2018-06-29-上午10:27
 * @description TODO
 */
public class ThreadBeetl implements Runnable {
    private CommodityEntity commodityEntity;
    private GroupTemplate gt;

    public ThreadBeetl(CommodityEntity commodityEntity, GroupTemplate gt) {
        this.commodityEntity = commodityEntity;
        this.gt = gt;
    }

    public void run() {
        //将实体传入,进行操作之后传出值
        Template template = gt.getTemplate("/beetlTest.btl");
        template.binding("commodity", commodityEntity);
        String render = template.render();
        Double aDouble = Double.valueOf(render);
//        if ((commodityEntity.getPrice() + 1) != aDouble) {
            System.out.println(commodityEntity.getBookName() + ":" + (commodityEntity.getPrice() + 1) + "=?" + aDouble);
//        }

    }

    public void run1() {

        Map map = new HashMap();
        map.put("commodity", commodityEntity);
        Map ret;
        try {
            ret = gt.runScript("/beetlTest.script", map);
            Double aDouble = (Double) ret.get("return");
            System.out.println(commodityEntity.getBookName() + ":\n" + (commodityEntity.getPrice() + 1) + "=?" + aDouble);
        } catch (ScriptEvalError e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

    public CommodityEntity getCommodityEntity() {
        return commodityEntity;
    }

    public void setCommodityEntity(CommodityEntity commodityEntity) {
        this.commodityEntity = commodityEntity;
    }


}
