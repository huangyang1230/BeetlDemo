package com.hy.demo;

/**
 * @author ocean
 * @create 2018-06-29-上午10:30
 * @description TODO
 */
public class CommodityEntity {
    private String bookName;
    private double price;
    private String isbn;

    public CommodityEntity(String bookName, double price, String isbn) {
        this.bookName = bookName;
        this.price = price;
        this.isbn = isbn;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }
}
